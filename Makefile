##
# Project Title
#
# @file
# @version 0.1


run:
	poetry run python crawl


mypy:
	poetry run mypy crawl --disallow-untyped-def --install-types


format:
	poetry run isort crawl && poetry run black crawl


flake:
	poetry run flake8 crawl


complexity:
	poetry run xenon crawl


check: complexity format mypy flake


test:
	poetry run pytest crawl
# end
