# Crawler

## Install dependencies

### Prerequesite - install poetry

**Linux, OSx**

`curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -`


**Windows powershell**

`Invoke-WebRequest -Uri https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py -UseBasicParsing).Content | python -`


### Install dependencies

`poetry install`


## Usage

`poetry run python cli.py <root_url> <depth: int>`

By default the crawler will run with multiprocessing, reserving one process for parsing and other for crawling.

Output will be stored in a file `out.txt`, but you can override that with the flag `-o=other_file.txt`.

To see all available options use:
`poetry run python cli.py -h`


# About

This is a simple crawler that has two implementation:
 - one for running on a single thread (slow)
 - the other one for running on a multiple processes.
 
Crawler with multiprocessing uses one process to do parsing, extracting links and rank calculation,
the other processes are responsible for crawling links.

I used 3 distinct queues:
 - crawler queue, where we enqueue the links needed to be crawled,
 - parsing queues, where we enqueue html documents needed to be processed
 - and result queue, which is only used to communicate results back from the processing worker.
 
 As I used only one worker for processing, the implementation is pretty straightforward as we coninue
 processing html documents until the end of the queue for a specific depth.
 
