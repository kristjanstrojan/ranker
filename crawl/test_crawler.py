from urllib.parse import urlparse

from bs4 import BeautifulSoup

from . import utils


def test_parse_and_validate_url() -> None:
    current = urlparse("https://domain.com/a/b/c?parsed=true")

    assert (
        utils.parse_and_validate_url(
            current, "http://test.com:8000/a/b/c?q=1&w=2"
        )
        == "http://test.com:8000/a/b/c?q=1&w=2"
    )
    assert (
        utils.parse_and_validate_url(current, "//test.com:8000/a/b/c?q=1&w=2")
        == "https://test.com:8000/a/b/c?q=1&w=2"
    )
    assert (
        utils.parse_and_validate_url(current, "/a/b/c?q=1&w=2")
        == "https://domain.com/a/b/c?q=1&w=2"
    )


def test_calculate_rang() -> None:
    current_url = "https://www.foo.com/bar.html"
    urls = [
        "http://www.foo.com/a.html",
        "http://www.foo.com/b/c.html",
        "http://baz.foo.com/",
        "http://www.google.com/bar.html",
    ]

    rank = utils.calculate_rank(current_url, urls)

    assert rank == 0.5


def test_parse_links() -> None:
    current = "https://domain.com/a/b/c?parsed=true"

    html_doc = (
        "<html><body>"
        '<a href="http://test.com:8000/a/b/c?q=1&w=2"></a>'
        '<a href="//test.com:8000/a/b/c?q=1&w=2"></a>'
        '<a href="/a/b/c?q=1&w=2"></a>'
        "<body></html>"
    )

    links = utils.parse_links(current, BeautifulSoup(html_doc, "html.parser"))

    assert links[0] == "http://test.com:8000/a/b/c?q=1&w=2"
    assert links[1] == "https://test.com:8000/a/b/c?q=1&w=2"
    assert links[2] == "https://domain.com/a/b/c?q=1&w=2"
