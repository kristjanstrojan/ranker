import multiprocessing as mp
from typing import Dict, List, Tuple

import structlog
from bs4 import BeautifulSoup

from . import utils

logger = structlog.getLogger(__name__)


FINISHED = "__FINISHED__"


def start_crawler(root_url: str, max_depth: int, out_file: str) -> None:
    cores = mp.cpu_count()

    crawler_queue: mp.Queue = mp.Queue()
    parser_queue: mp.Queue = mp.Queue()
    result_queue: mp.Queue = mp.Queue()

    crawler_queue.put((1, root_url))

    crawlers: List[Tuple[int, mp.Process]] = []

    for i in range(cores - 1):
        crawler = mp.Process(
            target=crawler_worker,
            kwargs={
                "crawler_queue": crawler_queue,
                "parser_queue": parser_queue,
                "max_depth": max_depth,
                "cid": i,
            },
        )
        crawlers.append((i, crawler))
        crawler.start()
        logger.info("Crawler start", cid=i)

    parser = mp.Process(
        target=parser_worker,
        kwargs={
            "crawler_queue": crawler_queue,
            "parser_queue": parser_queue,
            "result_queue": result_queue,
            "max_depth": max_depth,
        },
    )
    parser.start()
    parser.join()

    # Here is a problem:
    #  - in the first iteration i tried to wait for all processes to finish,
    #    but they constantly hanged (assume there was some race condition with
    #    forking and acquiring lock).
    #  - moved results right after joining the parser worker
    #    and then just terminating other processes (seems to be working)
    results = result_queue.get()
    logger.info("Saving results")
    utils.save_results(results, out_file=out_file)

    for (cid, crawler) in crawlers:
        logger.info("Join Process", cid=cid)
        crawler.terminate()


def parser_worker(
    *,
    crawler_queue: mp.Queue,
    parser_queue: mp.Queue,
    result_queue: mp.Queue,
    max_depth: int
) -> int:
    results: Dict[str, Tuple[int, float]] = {}

    while True:
        try:
            item = parser_queue.get()
            if item == FINISHED:
                result_queue.put(results)
                logger.info("PARSER FINISHED")
                break

            (url, depth, html_doc) = item
            logger.info("PARSE", url=url, depth=depth)

            soup = BeautifulSoup(html_doc, "html.parser")
            links = utils.parse_links(url, soup)
            rank = utils.calculate_rank(url, links)

            if url not in results:
                results[url] = (depth, rank)

            if depth < max_depth:
                for link in links:
                    if link not in results:
                        logger.info(
                            "CRAWLER ENQUE", link=link, depth=depth + 1
                        )
                        crawler_queue.put((depth + 1, link))
            else:
                crawler_queue.put(FINISHED)
        except Exception as e:
            logger.exception(e)

    return 0


def crawler_worker(
    *,
    crawler_queue: mp.Queue,
    parser_queue: mp.Queue,
    max_depth: int,
    cid: int
) -> int:
    while True:
        try:
            item = crawler_queue.get()

            if item == FINISHED:
                logger.info("CRAWLER FINISHED", cid=cid)
                crawler_queue.put(FINISHED)
                parser_queue.put(FINISHED)
                logger.info("CRAWLER EXIT", cid=cid)
                break

            depth: int
            url: str
            (depth, url) = item

            logger.info("CRAWL", url=url, depth=depth, cid=cid)

            html_doc = utils.retrieve(url)

            parser_queue.put((url, depth, html_doc))

        except Exception as e:
            logger.error(e)

    return 0
