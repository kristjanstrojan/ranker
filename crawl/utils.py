from collections import Counter
from typing import List, Optional
from urllib.parse import ParseResult, urlparse

import requests
import structlog
from bs4 import BeautifulSoup

logger = structlog.get_logger(__name__)


class CannotRetrieveResourceError(Exception):
    pass


class InvalidUrlError(Exception):
    pass


def retrieve(url: str) -> str:
    """
    Retrieves the html doc from the url.
    """
    try:
        response = requests.get(url)
    except requests.exceptions.MissingSchema:
        raise InvalidUrlError

    if response.status_code != 200:
        raise CannotRetrieveResourceError(
            f"Status code: {response.status_code}"
        )

    html_doc = response.text

    return html_doc


def parse_links(current_url: str, soup: BeautifulSoup) -> List[str]:
    current_url_parsed = urlparse(current_url)

    links = []

    for link in soup.find_all("a"):
        if link.has_attr("href"):
            href = link.get("href")
            url = parse_and_validate_url(current_url_parsed, href)
            if url is not None:
                links.append(url)
            else:
                logger.warn(f"Cannot parse url '{href}'")

    return links


def calculate_rank(current_url: str, links: List[str]) -> float:
    current_hostname = urlparse(current_url).hostname
    counter = Counter([urlparse(link).hostname for link in links])

    count_current = counter.get(current_hostname)

    if count_current is None:
        return 0.0

    rank = round(count_current / counter.total(), 2)
    return rank


VALID_SCHEMES = ["http", "https", ""]


def parse_and_validate_url(
    current_url: ParseResult, url: str
) -> Optional[str]:
    parsed_url = urlparse(url)

    scheme = parsed_url.scheme

    if scheme not in VALID_SCHEMES:
        # URL is not valid
        return None

    if url.startswith("//"):
        return f"{current_url.scheme}:{url}"
    elif url.startswith("/"):
        # Relative paths -> append scheme and hostname
        return f"{current_url.scheme}://{current_url.hostname}{url}"

    return url


def save_results(results: dict, out_file: str) -> None:
    with open(out_file, "w") as f:
        f.write("url\tdepth\tratio\n")
        for url, val in results.items():
            (depth, rank) = val
            f.write(f"{url}\t{depth}\t{rank}\n")
