import multiprocessing as mp

from . import crawler, mproc

mp.set_start_method("spawn", force=True)


def run(
    root_url: str, depth: int, single: bool = False, out_file: str = None
) -> None:
    if single is True:
        crawler.start_crawler(root_url, depth, out_file=out_file or "out.txt")
    else:
        mproc.start_crawler(root_url, depth, out_file=out_file or "out.txt")
