from queue import Queue
from typing import List, Tuple

import structlog
from bs4 import BeautifulSoup

from . import utils

logger = structlog.getLogger(__name__)


def start_crawler(root_url: str, max_depth: int, out_file: str) -> None:

    INITIAL_DEPTH = 1

    logger.info("Starting crawler", root_url=root_url, max_depth=max_depth)

    results = {}
    queue: Queue = Queue()

    queue.put((INITIAL_DEPTH, root_url))

    while not queue.empty():
        (depth, url) = queue.get()

        # Assumption is that we want the link with the lowest depth.
        # if find a link in a higher depth, we ignore it.
        if url in results:
            continue

        logger.info("Parsed url", url=url, depth=depth)

        try:
            (rank, links) = parse_resource(depth, url)
        except utils.InvalidUrlError:
            logger.error("Invalid url", url=url)
            continue
        except utils.CannotRetrieveResourceError as err:
            logger.error("Cannot retrieve resource", url=url, error=str(err))
            continue
        except Exception as e:
            logger.exception(e)
            continue

        if url not in results:
            results[url] = (depth, rank)

        if depth >= max_depth:
            continue

        # Put links in queue
        for link in links:
            if link not in results:
                queue.put((depth + 1, link))

    utils.save_results(results, out_file=out_file)


def parse_resource(depth: int, raw_url: str) -> Tuple[float, List[str]]:
    res = BeautifulSoup(utils.retrieve(raw_url), "html.parser")
    links = utils.parse_links(raw_url, res)
    rank = utils.calculate_rank(raw_url, links)

    return (rank, links)
