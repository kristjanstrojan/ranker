import argparse

from crawl import run


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Calculate pagerank of a particular website."
    )
    parser.add_argument("url", type=str, help="Root url")
    parser.add_argument("depth", type=str, help="Depth limit for crawling")
    parser.add_argument(
        "--single",
        action=argparse.BooleanOptionalAction,
        help="Run crawler with multiprocessing",
    )
    parser.add_argument(
        "-o",
        "--out",
        action="store",
        dest="out_file",
        help="Override output file name (default: out.txt)"
    )
    args = parser.parse_args()
    run(args.url, int(args.depth), single=args.single, out_file=args.out_file)
